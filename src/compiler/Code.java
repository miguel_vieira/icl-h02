package compiler;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Code {

	List<String> code;
	
	public Code() {
		this.code = new ArrayList<String>();
	}
	
	public void emit(String code) {
		this.code.add(code);
	}
	
	public void dump(String filename) {
		Path file = Paths.get(filename);
		try {
			Files.write(file, this.code, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void show() {
		for (String command : code)
			System.out.println(command);
	}
	
	public void clear() {
		this.code = new ArrayList<String>();
	}
}

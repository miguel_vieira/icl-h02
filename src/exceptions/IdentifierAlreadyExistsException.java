package exceptions;

public class IdentifierAlreadyExistsException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public IdentifierAlreadyExistsException() { 
		super(); 
	}
	
	public IdentifierAlreadyExistsException(String message) { 
		super(message); 
	}
	
	public IdentifierAlreadyExistsException(String message, Throwable cause) { 
		super(message, cause); 
	}
	
	public IdentifierAlreadyExistsException(Throwable cause) { 
		super(cause); 
	}

}

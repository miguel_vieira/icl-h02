package exceptions;

public class IdentifierNotDeclaredException extends Exception{
	private static final long serialVersionUID = 1L;

	public IdentifierNotDeclaredException() { 
		super(); 
	}
	
	public IdentifierNotDeclaredException(String message) { 
		super(message); 
	}
	
	public IdentifierNotDeclaredException(String message, Throwable cause) { 
		super(message, cause); 
	}
	
	public IdentifierNotDeclaredException(Throwable cause) { 
		super(cause); 
	}
}

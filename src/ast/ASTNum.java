package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.TypeError;
import typechecker.ASTTypeInt;
import typechecker.IType;
/*
import core.Environment;
import evaluator.IValue;
import evaluator.VInt;
import exceptions.TypeError;
import typechecker.ASTTypeInt;
import typechecker.IType;
*/

public class ASTNum implements ASTNode{

	private int value;
	
	public ASTNum(int value) {
		this.value = value;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError {
		return new ASTTypeInt(this.value);
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError {
		
		if (!(type instanceof ASTTypeInt))
			throw new TypeError("Wrong type for operand int, should be ASTTypeInt instead is: " + type.getClass().getSimpleName());
		
		code.emit("sipush " + ((ASTTypeInt) type).get());
	}

}

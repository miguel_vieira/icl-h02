package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.ASTTypeT;
import typechecker.IType;

public class ASTId implements ASTNode{
	
	private String id;
	
	public ASTId(String id) {
		this.id = id;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException {
		return e.find(this.id);
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError {
		
		if (offset >= level)
			throw new TypeError("Illegal arguments to id operator.");
		
		if (level == 0)
			return;
		
		code.emit("aload 4");
		
		if (level == 1) {
			if (type instanceof ASTTypeInt || type instanceof ASTTypeBool)
				code.emit("getfield frame_0/x0 I");
			else if (type instanceof ASTTypeT)
				code.emit("getfield frame_0/x0 Ljava/lang/Object;");
			else throw new TypeError("Illegal arguments to id operator.");
			return;
		}
		
		int curr_offset = offset;
		for (int i = level - 1; i >= 0; i--) {
			if (curr_offset == 0) {
				if (type instanceof ASTTypeInt || type instanceof ASTTypeBool)
					code.emit("getfield frame_" + String.valueOf(i) + "/x0 I");
				else if (type instanceof ASTTypeT)
					code.emit("getfield frame_" + String.valueOf(i) + "/x0 Ljava/lang/Object;");
				else throw new TypeError("Illegal arguments to id operator.");
				break;
			}
			code.emit("getfield frame_" + String.valueOf(i) + "/sl Lframe_" + String.valueOf(i - 1) + ";");
			curr_offset --;
		}
	}

}

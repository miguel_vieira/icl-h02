package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.ASTTypeT;
import typechecker.IType;

public class ASTAssign implements ASTNode{

	private ASTNode left, right;
	
	public ASTAssign(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = this.left.typecheck(e);
		if (t1 instanceof ASTTypeT) {
			return this.right.typecheck(e);
		}
		throw new TypeError("Illegal arguments to := operator.");
	}
	
	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		// [[E1]]D
		this.left.compile(bridge, e, code, level, offset, type);
		
		if (type instanceof ASTTypeInt)
			code.emit("checkcast ref_int");
		else if (type instanceof ASTTypeBool)
			code.emit("checkcast ref_bool");
		else if (type instanceof ASTTypeT)
			code.emit("checkcast ref_class");
		else
			throw new TypeError("Invalid Type for operand :=");
		
		// [[E2]]D
		this.right.compile(bridge, e, code, level, offset, type);
		
		if (type instanceof ASTTypeInt)
			code.emit("putfield ref_int/v I");
		else if (type instanceof ASTTypeBool)
			code.emit("putfield ref_bool/v I");
		else if (type instanceof ASTTypeT)
			code.emit("putfiels ref_class/v Ljava/lang/Object");
		else
			throw new TypeError("Invalid Type for operand :=");
	}

}

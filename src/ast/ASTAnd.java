package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.IType;

public class ASTAnd implements ASTNode{

	private ASTNode left, right;
	
	public ASTAnd(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = left.typecheck(e);
		if (t1 instanceof ASTTypeBool) {
			IType t2 = right.typecheck(e);
			if (t2 instanceof ASTTypeBool)
				return new ASTTypeBool(((ASTTypeBool) t1).get() && ((ASTTypeBool) t2).get());
		}
		throw new TypeError("Illegal arguments to && operator.");
	}
	
	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		if (!(type instanceof ASTTypeBool))
			throw new TypeError("Wrong type for operand &&, should be ASTTypeBool instead is: " + type.getClass().getSimpleName());
		
		// [[E1]]D
		IType type1 = this.left.typecheck(e);
		this.left.compile(bridge, e, code, level, offset, type1);
		// [[E2]]D
		IType type2 = this.right.typecheck(e);
		this.right.compile(bridge, e, code, level, offset, type2);
		code.emit("iand");
	}

}

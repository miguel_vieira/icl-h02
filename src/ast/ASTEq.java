package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.IType;

public class ASTEq implements ASTNode{
	
	private ASTNode left, right;
	
	public ASTEq(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = left.typecheck(e);
		if (t1 instanceof ASTTypeInt) {
			IType t2 = right.typecheck(e);
			if (t2 instanceof ASTTypeInt)
				return new ASTTypeBool(((ASTTypeInt) t1).get() == ((ASTTypeInt) t2).get());
		}
		else if (t1 instanceof ASTTypeBool) {
			IType t2 = right.typecheck(e);
			if (t2 instanceof ASTTypeBool)
				return new ASTTypeBool(((ASTTypeBool) t1).get() == ((ASTTypeBool) t2).get());
		}
		throw new TypeError("Illegal arguments to == operator.");
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		if (!(type instanceof ASTTypeInt) && !(type instanceof ASTTypeBool))
			throw new TypeError("Wrong type for operand ==, should be ASTTypeInt or ASTTypeBool instead is " + type.getClass().getSimpleName());
		
		// [[E1]]D
		IType type1 = this.left.typecheck(e);
		this.left.compile(bridge, e, code, level, offset, type1);
		// [[E2]]D
		IType type2 = this.right.typecheck(e);
		this.right.compile(bridge, e, code, level, offset, type2);
		
		code.emit("isub");
		code.emit("ifeq L1");
		code.emit("sipush 0");
		code.emit("goto L2");
		code.emit("L1:");
		code.emit("sipush 1");
		code.emit("L2:");
	}
}

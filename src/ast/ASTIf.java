package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.IType;

public class ASTIf implements ASTNode{
	
	private ASTNode left, center, right;
	
	public ASTIf(ASTNode left, ASTNode center, ASTNode right) {
		this.left = left;
		this.center = center;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = this.left.typecheck(e);
		if (t1 instanceof ASTTypeBool) {
			if (((ASTTypeBool) t1).get() == true)
				return this.center.typecheck(e);
			else
				return this.right.typecheck(e);
		}
		throw new TypeError("Illegal arguments to if operator.");
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		// [[E1]]D
		IType type1 = this.left.typecheck(e);
		this.left.compile(bridge, e, code, level, offset, type1);
		
		code.emit("ifeq L1");
		
		// [[E2]]D
		IType type2 = this.center.typecheck(e);
		this.center.compile(bridge, e, code, level, offset, type2);
		
		code.emit("goto L2");
		code.emit("L1:");
		
		// [[E3]]D
		IType type3 = this.right.typecheck(e);
		this.right.compile(bridge, e, code, level, offset, type3);
		
		code.emit("L2:");
	}

}

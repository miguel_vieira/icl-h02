package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.IType;

public class ASTBool implements ASTNode{
	
	private boolean value;
	
	public ASTBool(boolean value) {
		this.value = value;
	}

	@Override
	public IType typecheck(Environment<IType> e) throws TypeError {
		return new ASTTypeBool(this.value);
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError {
		
		if (!(type instanceof ASTTypeBool))
			throw new TypeError("Wrong type for operand bool, should be ASTTypeBool instead is: " + type.getClass().getSimpleName());
		
		if (this.value == true)
			code.emit("sipush 1");
		else
			code.emit("sipush 0");
	}

}

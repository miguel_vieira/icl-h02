package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.ASTTypeT;
import typechecker.IType;

public class ASTDeref implements ASTNode{
	
	ASTNode expression;
	
	public ASTDeref(ASTNode expression) {
		this.expression = expression;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = this.expression.typecheck(e);
		if (t1 instanceof ASTTypeT) {
			return ((ASTTypeT) t1).get();
		}
		throw new TypeError("Illegal arguments to ! operator.");
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code,  int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		// [[E]]D
		IType type1 = this.expression.typecheck(e);
		this.expression.compile(bridge, e, code, level, offset, type1);
		
		if (type1 instanceof ASTTypeInt) {
			code.emit("checkcast ref_int");
			code.emit("getfield ref_int/v I");
		}
		else if (type1 instanceof ASTTypeBool) {
			code.emit("checkcast ref_bool");
			code.emit("getfield ref_bool/v I");
		}
		else if (type1 instanceof ASTTypeT) {
			code.emit("checkcast ref_class");
			code.emit("getfield ref_class/v Ljava/lang/Object");
		}
		else throw new TypeError("Invalid type for operand !");
	}

}

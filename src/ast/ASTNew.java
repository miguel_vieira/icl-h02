package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.ASTTypeT;
import typechecker.IType;

public class ASTNew implements ASTNode{
	
	ASTNode expression;
	
	public ASTNew(ASTNode expression) {
		this.expression = expression;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = this.expression.typecheck(e);
		if (t1 instanceof ASTTypeInt || t1 instanceof ASTTypeBool || t1 instanceof ASTTypeT) {
			return new ASTTypeT(t1);
		}
		throw new TypeError("Illegal arguments to new operator.");
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		if (!(type instanceof ASTTypeT))
			throw new TypeError("Wrong type for operand new, should be ASTTypeT instead is: " + type.getClass().getSimpleName());
		
		IType type1 = this.expression.typecheck(e);
		if (type1 instanceof ASTTypeInt) {
			code.emit("new ref_int");
			code.emit("dup");
			code.emit("invokespecial ref_int/<init>()V");
			code.emit("dup");
			// [[E]]D
			this.expression.compile(bridge, e, code, level, offset, type1);
			code.emit("putfield ref_int/value I");
		}
		else if (type1 instanceof ASTTypeBool) {
			code.emit("new ref_bool");
			code.emit("dup");
			code.emit("invokespecial ref_bool/<init>()V");
			code.emit("dup");
			// [[E]]D
			this.expression.compile(bridge, e, code, level, offset, type1);
			code.emit("putfield ref_bool/value I");
		}
		else if (type1 instanceof ASTTypeT) {
			code.emit("new ref_class");
			code.emit("dup");
			code.emit("invokespecial ref_class/<init>()V");
			code.emit("dup");
			// [[E]]D
			this.expression.compile(bridge, e, code, level, offset, type1);
			code.emit("putfield ref_class/value Ljava/lang/Object");
		}
		
	}

}

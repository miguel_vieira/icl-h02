package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.ASTTypeInt;
import typechecker.ASTTypeT;
import typechecker.IType;

public class ASTLet implements ASTNode{
	
	private String id;
	private ASTNode left, right;
	
	public ASTLet(String id, ASTNode left, ASTNode right) {
		this.id = id;
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		e = e.beginScope();
		IType type1 = this.left.typecheck(e);
		e.assoc(this.id, type1);
		IType type2 = this.right.typecheck(e);
		e = e.endScope();
		return type2;
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		e.beginScope();
		
		// Constructor
		code.emit("new frame_" + level);
		code.emit("dup");
		code.emit("invokespecial frame_" + level + "/<init>()V");
		code.emit("dup");
		code.emit("aload 4");
		if (level == 0)
			code.emit("putfield frame_" + level + "/sl Ljava/lang/Object;");
		else
			code.emit("putfield frame_" + level + "/sl Lframe_" + String.valueOf(level - 1) + ";");
		code.emit("astore 4");
		
		// Variable
		code.emit("aload 4");
		IType type1 = this.left.typecheck(e);
		e.assoc(this.id, type1);
		this.left.compile(bridge, e, code, level + 1, offset, type);
		if (type1 instanceof ASTTypeInt || type1 instanceof ASTTypeBool) {
			code.emit("putfield frame_" + level + "/x" + offset + " I");
			bridge.generateFrame(level, "I");
		}
		else if (type1 instanceof ASTTypeT) {
			code.emit("putfield frame_" + level + "/x" + offset + " Ljava/lang/Object;");
			bridge.generateFrame(level, "Ljava/lang/Object;");
		}
		else throw new TypeError("Illegal arguments to let operator.");
		
		// Expression
		IType type2 = this.right.typecheck(e);
		this.right.compile(bridge, e, code, level + 1, offset, type2);
		
		code.emit("aload 4");
		code.emit("getfield frame_" + level + "/sl Ljava/lang/Object;");
		code.emit("astore 4");
		
		e.endScope();
	}

}

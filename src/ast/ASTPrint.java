package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.IType;

public class ASTPrint implements ASTNode{
	
	private ASTNode value;
	
	public ASTPrint(ASTNode value) {
		this.value = value;
	}

	@Override
	public IType typecheck(Environment<IType> e)
			throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type)
			throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		// TODO Auto-generated method stub
		
	}
	
}

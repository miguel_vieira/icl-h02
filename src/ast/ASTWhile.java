package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.ASTTypeBool;
import typechecker.IType;

public class ASTWhile implements ASTNode{
	
	private ASTNode left, right;
	
	public ASTWhile(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}
	
	@Override
	public IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		IType t1 = this.left.typecheck(e);
		if (t1 instanceof ASTTypeBool) {
			return t1;
		}
		throw new TypeError("Illegal arguments to while operator.");
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		
		if (!(type instanceof ASTTypeBool))
			throw new TypeError("Wrong type for operand while, should be ASTTypeBool instead is: " + type.getClass().getSimpleName());
		
		code.emit("L1:");
		// [[E1]]D
		IType type1 = this.left.typecheck(e);
		this.left.compile(bridge, e, code, level, offset, type1);
		code.emit("ifeq L2");
		// [[E2]]D
		IType type2 = this.right.typecheck(e);
		this.right.compile(bridge, e, code, level, offset, type2);
		code.emit("goto L1");
		code.emit("L2:");
	}

}

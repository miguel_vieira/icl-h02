package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.IType;

public interface ASTNode {
	
	IType typecheck(Environment<IType> e) throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException;

	void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type)
			throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException;

}

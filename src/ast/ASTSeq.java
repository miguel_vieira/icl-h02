package ast;

import compiler.Code;
import core.Environment;
import core.JasminBridge;
import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;
import exceptions.TypeError;
import typechecker.IType;

public class ASTSeq implements ASTNode{
	
	private ASTNode left, right;
	
	public ASTSeq(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public IType typecheck(Environment<IType> e)
			throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compile(JasminBridge bridge, Environment<IType> e, Code code, int level, int offset, IType type)
			throws TypeError, IdentifierNotDeclaredException, IdentifierAlreadyExistsException {
		// TODO Auto-generated method stub
		
	}
	
	

}

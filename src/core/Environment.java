package core;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;

import exceptions.IdentifierAlreadyExistsException;
import exceptions.IdentifierNotDeclaredException;

public class Environment<IType> {
	
	private Stack<Map<String, IType>> ast;
	
	public Environment(){
		this.ast = new Stack<Map<String, IType>>();
		this.ast.push(new HashMap<String, IType>());
	}
	
	public Environment<IType> beginScope(){
		this.ast.push(new HashMap<String, IType>());
		return this;
	}
	
	public Environment<IType> endScope(){
		this.ast.pop();
		return this;
	}
	
	public void assoc(String id, IType value) throws IdentifierAlreadyExistsException{
		Map<String, IType> current_level = this.ast.peek();
		if (current_level.containsKey(id))
			throw new IdentifierAlreadyExistsException();
		current_level.put(id, value);
	}
	
	public IType find(String id) throws IdentifierNotDeclaredException{
		ListIterator<Map<String, IType>> iter = this.ast.listIterator(this.ast.size());
		while(iter.hasPrevious()){
			Map<String, IType> current_level = iter.previous();
			if (current_level.containsKey(id))
				return current_level.get(id);
		}
		throw new IdentifierNotDeclaredException();
	}
}

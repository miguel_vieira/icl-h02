package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import compiler.Code;

public class JasminBridge {
	
	private String projectFilePath;
	private final String tempFilePath;
	
	public JasminBridge(String projectFilePath) {
		this.projectFilePath = projectFilePath;
		this.tempFilePath = this.projectFilePath + "\\temp";
	}
	
	private void generateRefInt() {
		File ref_int = new File(this.tempFilePath + "\\ref_int.j");
		try {
			FileWriter writer = new FileWriter(ref_int);
			writer.write("\n.class ref_int");
			writer.write("\n.super java/lang/Object");
			writer.write("\n.field public value I");
			writer.write("\n.method public <init>()V");
			writer.write("\naload_0");
			writer.write("\ninvokenonvirtual java/lang/Object/<init>()V");
			writer.write("\nreturn");
			writer.write("\n.end method");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void generateFrame(int level, String type) {
		File ref_int = new File(this.tempFilePath + "\\frame_" + level + ".j");
		try {
			FileWriter writer = new FileWriter(ref_int);
			writer.write("\n.class frame_" + level);
			writer.write("\n.super java/lang/Object");
			if (level == 0)
				writer.write("\n.field public sl Ljava/lang/Object;");
			else
				writer.write("\n.field public sl Lframe_" + level + ";");
			writer.write("\n.field public x0 " + type);
			writer.write("\n.method public <init>()V");
			writer.write("\naload_0");
			writer.write("\ninvokenonvirtual java/lang/Object/<init>()V");
			writer.write("\nreturn");
			writer.write("\n.end method");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteTemp() {
		File dir = new File(this.tempFilePath);
		
		if(dir.isDirectory() && dir.exists()) {
			File[] listFiles = dir.listFiles();
			for(File file : listFiles){
				file.delete();
			}
			dir.delete();
		}
	}
	
	public void createTemp() {
		File dir = new File(this.tempFilePath);
		dir.mkdir();
	}
	
	public String compile(Code code) {
		
		this.generateRefInt();
		
		code.dump(this.tempFilePath + "\\Test.txt");
		code.clear();
		File file_code = new File(this.tempFilePath + "\\Test.txt");
		File file_j = new File(this.tempFilePath + "\\Test.j");
		
		try {
			FileWriter writer = new FileWriter(file_j);
			writer.write("\n.class public Test");
			writer.write("\n.super java/lang/Object");
			writer.write("\n.method public <init>()V");
			writer.write("\naload_0");
			writer.write("\ninvokenonvirtual java/lang/Object/<init>()V");
			writer.write("\nreturn");
			writer.write("\n.end method");
			writer.write("\n.method public static main([Ljava/lang/String;)V");
			writer.write("\n.limit locals 10");
			writer.write("\n.limit stack 256");
			writer.write("\ngetstatic java/lang/System/out Ljava/io/PrintStream;");
			/* Put generated code here */
			Scanner scanner = new Scanner(file_code);
			scanner.useDelimiter("\n");
			while (scanner.hasNext()) {
			    writer.write("\n" + scanner.next());
			}
			scanner.close();
			writer.write("\ninvokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
			writer.write("\ninvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
			writer.write("\nreturn");
			writer.write("\n.end method");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Runtime.getRuntime().exec("java -jar " + this.projectFilePath + "\\jasmin-2.4\\jasmin.jar "  + this.tempFilePath + "\\Test.j -d " + this.tempFilePath);
		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}
		
		File file_class = new File(this.tempFilePath + "\\Test.class");
		while(!file_class.exists())
			continue;
		
		String s1 = null;
		String s2 = null;
		try {
			Process p = Runtime.getRuntime().exec("java -cp " + this.tempFilePath + " Test");
			
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
 
			// read the output from the command
			/*
			while (true) {
				s1 = stdInput.readLine();
				if (s1 != null)
					break;
			}
			*/
			s1 = stdInput.readLine();
 
			// read any errors from the attempted command
			while ((s2 = stdError.readLine()) != null) {
				System.out.println(s2);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		/*
		if(dir.isDirectory() && dir.exists()) {
			File[] listFiles = dir.listFiles();
			for(File file : listFiles){
				file.delete();
			}
			dir.delete();
		}
		*/
		
		return s1;
	}

}

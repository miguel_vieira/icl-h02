package core;

public class Triple<T1, T2, T3> {

	private T1 left;
	private T2 center;
	private T3 right;
	
	public Triple(T1 left, T2 center, T3 right) {
		this.left = left;
		this.center = center;
		this.right = right;
	}
	
	public T1 getLeft() {
		return this.left;
	}
	
	public T2 getCenter() {
		return this.center;
	}
	
	public T3 getRight() {
		return this.right;
	}
}

//options { DEBUG_PARSER = true; }

PARSER_BEGIN(Parser)

package parser;
import ast.*;
import compiler.*;
import core.*;
import typechecker.*;
/** ID lister. */
public class Parser {

  /** Main entry point. */
  public static void main(String args[]) {
    Parser parser = new Parser(System.in);
    JasminBridge jb = new JasminBridge(System.getProperty("user.dir"));
    Environment environment = new Environment();
    Code code = new Code();
    ASTNode exp;

    while (true) {
	    try {
	      	
		    exp = parser.Start();
		    jb.deleteTemp();
	      	jb.createTemp();
		    IType type = exp.typecheck(environment);
		    code.emit("aconst_null");
		    code.emit("astore 4");
		    exp.compile(jb, environment, code, 0, 0, type);
		    System.out.println ("##########");
		    System.out.println ("Code:");
		    System.out.println ("##########");
		    code.show();
		    String result = jb.compile(code);
		    System.out.println ("##########");
		    System.out.println ("Result: " + result);
		    System.out.println ("##########");
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	parser.ReInit(System.in);
	    }
    }
  }

}

PARSER_END(Parser)

SKIP :
{
  " "
| "\t"
| "\r"
| "\n"
}

TOKEN :
{
  < PRINT : "println" >
  | 
  < LET : "let" >
  |
  < IN : "in" >
  |
  < END : "end" >
  |
  <  FUN : "fun" >
  |
  < NEW : "new" >
  |
  < IF : "if" >
  |
  < THEN : "then" >
  |
  < ELSE: "else" >
  |
  < WHILE : "while" >
  |
  < DO : "do" >
  | 
  < BOOL : "false" | "true" >
  | 
  < ID: ["a"-"z","A"-"Z"] ( ["a"-"z","A"-"Z","0"-"9"] )* >
  |
  < NUM: (["0"-"9"]) + >
  | 
  < PLUS : "+" >
  |
  < MINUS : "-">
  |
  < MUL : "*">
  |
  < DIV : "/">
  |
  < LPAR : "(" >
  |
  < RPAR : ")" >
  |
  < ASSIGN : ":=" >
  |   
  < EL : ";;" >
  |
  < SEQ : ";" >
  |
  < EQ : "==" >
  |
  < AND : "&&" >
  |
  < OR : "||" >
  | 
  < GREATER : ">" >
  |
  < GREATEREQUAL : ">=" >
  |
  < LESSEQUAL : "<=" >
  | 
  < LESS : "<" >
  |
  < COMMA : "," >
  |
  < ARROW : "->" >
  |
  < EXCLAMATION : "!" >
  |
  <  CREATE : "=" >
  |
  < DEFINE : ":" >
  |
  < TYPE : "int" | "bool" >
  
}

ASTNode Start():
{ ASTNode e; }
{
   e = EM() <EL>
   { return e; }
}

ASTNode EM():
{ Token n;
  ASTNode e1, e2; }
{
  	e1 = E() ( < SEQ > e2 = EM() { e1 = new ASTSeq(e1, e2);})* { return e1; }
}


ASTNode E():
{ ASTNode e1, e2; }
{
    e1 = EA() (
    	< EQ > e2 = EA() { e1 = new ASTEq(e1, e2);}
		| 
    	< GREATER > e2 = EA() { e1 = new ASTGreater(e1, e2); }
   		|
   		< GREATEREQUAL > e2 = EA() { e1 = new ASTGreaterEqual(e1, e2); }
 		|
 		< LESSEQUAL > e2 = EA() { e1 = new ASTLessEqual(e1, e2); }
   		|
   		< LESS > e2 = EA() { e1 = new ASTLess(e1, e2); }
   		|
   		< AND > e2 = EA() { e1 = new ASTAnd(e1, e2); }
   		|
   		< OR > e2 = EA() { e1 = new ASTOr(e1, e2); }  
    )? { return e1; }
}

ASTNode EA():
{ Token op;
  ASTNode e1, e2; }
{
  	e1 = T() ( (op = < PLUS > | op = < MINUS >) e2 = EA() { if (op.kind == PLUS) e1 = new ASTAdd(e1, e2); else e1 = new ASTSub(e1, e2);})* { return e1; }
}

ASTNode T():
{ Token op;
  ASTNode e1, e2; }
{
	e1 = F() 
	(
	  	< ASSIGN > e2 = E() { e1 = new ASTAssign(e1, e2);}
	    | 
		((op = < MUL > | op = < DIV >) e2 = T() { if (op.kind == MUL) e1 = new ASTMul(e1, e2); else e1 = new ASTDiv(e1, e2);})*
	)
	{ return e1; }
}

ASTNode AL():
{ ASTNode e1, e2;}
{
	( e1 = EM() ( < COMMA > e2 = EM() { e1 = new ASTApply(e1, e2); })*{ return e1; } )?
}

ASTNode PL():
{ Token n, t;
  ASTNode e1, e2; }
{
  ( n = < ID > <DEFINE > t = < TYPE > { return new ASTId(n.image); })?
}

ASTNode F():
{ Token n, t;
  ASTNode e1, e2, e3; }
{
	n = < NUM > { return new ASTNum(Integer.parseInt(n.image));} 
	|
	n = < ID > { return new ASTId(n.image); }
	|
	n = < BOOL > { return new ASTBool(Boolean.parseBoolean(n.image)); }
	|
	< LET > n = < ID > < CREATE > e1 = EM() < IN > e2 = EM() < END > { return new ASTLet(n.image, e1, e2); }
	|
	< LPAR > e1 = EM() < RPAR > { return e1; }
	|
	< NEW > e1 = F() { return new ASTNew(e1); }
	|
	< EXCLAMATION > e1 = F() { return new ASTDeref(e1); }
	|
	< IF > e1 = EM() < THEN > e2 = EM() < ELSE > e3 = EM() < END > { return new ASTIf(e1, e2, e3); }
	|
	< WHILE > e1 = EM() < DO > e2 = EM() < END > { return new ASTWhile(e1, e2); }
    |
    < PRINT > e1 = E() { return new ASTPrint(e1); } 
}




















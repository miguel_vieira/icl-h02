package typechecker;

public class ASTTypeT implements IType{
	
	private IType value;
	
	public ASTTypeT(IType value) {
		this.value = value;
	}
	
	public IType get() {
		return this.value;
	}

}

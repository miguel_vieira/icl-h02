package typechecker;

public class ASTTypeInt implements IType{

	private int value;
	
	public ASTTypeInt(int value) {
		this.value = value;
	}
	
	public int get() {
		return this.value;
	}

}

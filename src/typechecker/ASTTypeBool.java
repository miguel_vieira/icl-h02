package typechecker;

public class ASTTypeBool implements IType{
	
	private boolean value;
	
	public ASTTypeBool(boolean value) {
		this.value = value;
	}
	
	public boolean get() {
		return this.value;
	}
}

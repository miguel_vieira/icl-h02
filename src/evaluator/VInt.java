package evaluator;

public class VInt implements IValue{
	
	private int value;
	
	public VInt(int value) {
		this.value = value;
	}
	
	public int get() {
		return this.value;
	}

	@Override
	public void show() {
		System.out.println(this.value);
	}

}

package evaluator;

public class VCell implements IValue{
	
	private IValue value;
	
	public VCell(IValue value) {
		this.value = value;
	}
	
	public IValue get() {
		return this.value;
	}
	
	public void set(IValue value) {
		this.value = value;
	}

	@Override
	public void show() {
		System.out.println(this.value);
	}

}

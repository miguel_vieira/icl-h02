package evaluator;

public class VBool implements IValue{
	
	private boolean value;
	
	public VBool(boolean value) {
		this.value = value;
	}
	
	public boolean get() {
		return this.value;
	}

	@Override
	public void show() {
		System.out.println(this.value);
	}

}
